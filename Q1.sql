﻿create database itemfile DEFAULT CHARACTER SET utf8;
use itemfile;
create table item_category;
create table item_category(
	category_id int primary key auto_increment,
	category_name varchar(256) not null
);
